#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_debug.h"

// this will be called after main
void __attribute__ ((constructor)) initSystem()
{
  disastrOS_init();
}


int main(int argc, char** argv){
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue

  // spawn an init process
  printf("start\n");
  disastrOS_start();
  disastrOS_printStatus();



  // below a sequence of actions performed
  // by the running process
  // process switch might occur as a consequence of
  // a preempt action
  // or a system call
  
  // now we are in init
  // we pretend to fork
  printf("fork ");
  int fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();

  // we are still in the parent process;
  // we switch context;
  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();

  //we exit from child process
  disastrOS_exit(-1);
  disastrOS_printStatus();

  //parent reads value from child
  long retval=0;
  int wait_result=disastrOS_wait(0, &retval);
  printf("returned pid:%d, value:%ld \n", wait_result, retval);
  disastrOS_printStatus();

  // parent forks three times
  printf("fork ");
  fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();
  printf("fork ");
  fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();
  printf("fork ");
  fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();

  // parent waits for process 2
  printf("wait 2 ");
  wait_result=disastrOS_wait(2, &retval);
  disastrOS_printStatus();

  // we are still in the parent process;
  // we switch context;
  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();


  printf("fake wait \n");
  wait_result=disastrOS_wait(0, &retval);
  printf("returned pid:%d, value:%ld \n", wait_result, retval);
  disastrOS_printStatus();

  printf("fork ");
  fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();
  printf("fork ");
  fork_result = disastrOS_fork();
  printf(" child pid: %d\n", fork_result);
  disastrOS_printStatus();

  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();
  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();

  printf("exit\n");
  disastrOS_exit(-1);
  disastrOS_printStatus();

  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();
  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();
  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();

  printf("exit\n");
  disastrOS_exit(-1);
  disastrOS_printStatus();

  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();
  
  printf("wait alive process \n");
  wait_result=disastrOS_wait(4, &retval);
  disastrOS_printStatus();

  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();

  printf("preempt \n");
  disastrOS_preempt();
  disastrOS_printStatus();

  printf("exit\n");
  disastrOS_exit(-1);
  disastrOS_printStatus();

  printf("wait any process \n");
  wait_result=disastrOS_wait(0, &retval);
  disastrOS_printStatus();

  
  
  return 0;
}
