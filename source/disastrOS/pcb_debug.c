#include <stdio.h>
#include "pcb.h"


void PCBPtrList_print(ListHead* head) {
  ListItem* aux=head->first;
  printf("(");
  while(aux){
    PCBPtr* pcb_ptr= (PCBPtr*) aux;
    printf("%d", pcb_ptr->pcb->pid);
    aux=aux->next;
    if (aux)
      printf(", ");
  }
  printf(")");
}

void PCB_print(PCB* pcb){
  printf("[pid: %d, child: ", pcb->pid);
  PCBPtrList_print(&pcb->children);
  printf("]");
}

void PCBList_print(ListHead* head) {
  ListItem* aux=head->first;
  printf("{");
  while(aux){
    PCB* pcb= (PCB*) aux;
    PCB_print(pcb);
    aux=aux->next;
    if (aux)
      printf(", ");
  }
  printf("}");
}

