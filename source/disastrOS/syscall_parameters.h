#pragma once
#include <assert.h>
#include "cpu_status.h"
extern CPUState my_cpu;

static inline long getSyscallArgument(int argnum) {
  switch(argnum){
  case 0: return running->cpu_state.rax;
  case 1: return running->cpu_state.rdi;
  case 2: return running->cpu_state.rsi;
  case 3: return running->cpu_state.rdx;
  case 4: return running->cpu_state.rcx;
  case 5: return running->cpu_state.r8;
  case 6: return running->cpu_state.r9;
  case 7: return running->cpu_state.r10;
  default: assert(0 && "INVALID_ARGUMENT"); return -1;
  }
}

static inline void setSyscallArgument(int argnum, long argval) {
  switch(argnum){
  case 0: my_cpu.rax=argval; return;
  case 1: my_cpu.rdi=argval; return;
  case 2: my_cpu.rsi=argval; return;
  case 3: my_cpu.rdx=argval; return;
  case 4: my_cpu.rcx=argval; return;
  case 5: my_cpu.r8=argval;  return;
  case 6: my_cpu.r9=argval;  return;
  case 7: my_cpu.r10=argval; return;
  default: assert(0 && "INVALID_ARGUMENT"); 
  }
}


static inline void setSyscallRetvalue(PCB* pcb, long value) {
  pcb->cpu_state.rax=value;
}

static inline int getSyscallRetvalue() {
  return my_cpu.rax;
}
