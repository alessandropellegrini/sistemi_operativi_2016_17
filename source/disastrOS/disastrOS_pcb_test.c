#include <stdio.h>
#include "disastrOS.h"
#include "pcb_debug.h"

// this will be called after main
void __attribute__ ((constructor)) initSystem()
{
  disastrOS_init();
}

#define NUM_PROCESSES 10



int main(int argc, char** argv){
  // we create three processes
  // the first is in the running variable
  // the others are in the ready queue
  running=PCB_alloc();
  running->status=Running;

  for (int i=0; i<NUM_PROCESSES; ++i){
    PCB* new_pcb=PCB_alloc();
    printf("new pcb %p\n", new_pcb);
    new_pcb->status=Ready;

    // we put this process in the ready queue
    List_insert(&ready_list, ready_list.last, (ListItem*) new_pcb);
  }

  // we print the list of pcbs;
  PCBList_print(&ready_list);
  printf("\n");


 
  return 0;
}
