#pragma once

#include "pcb.h"
#include "double_linked_list.h"

extern PCB* init_pcb;
extern PCB* running;
extern int last_pid;
extern ListHead ready_list;
extern ListHead waiting_list;
extern ListHead zombie_list;

// a resource can be a device, a file or an ipc thing
extern ListHead resources_list;

// initializes the static structures
// initializes the syscall table
void disastrOS_init();

// spawns a fake init process
void disastrOS_start();
  
// generic syscall
long disastrOS_syscall(long syscall_num, ...);

// classical process control
long disastrOS_fork();
void disastrOS_exit(long exit_value);
long disastrOS_wait(long pid, long* retval);
void disastrOS_preempt();
