#pragma once
#include "disastrOS.h"

inline long getSyscallArgument(int argnum) {
  return running->cpu_state.registers[argnum];
}

inline void setSyscallRetvalue(PCB* pcb, long value) {
  pcb->cpu_state.registers[0]=value;
}

void internal_preempt();

void internal_fork();

void internal_exit();

void internal_wait();
