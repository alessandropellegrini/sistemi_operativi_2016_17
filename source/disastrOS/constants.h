#pragma once

#define MAX_NUM_PROCESSES 1024


// signals
#define MAX_SIGNALS 32
#define DSOS_SIGCHLD 0x1
#define DSOS_SIGHUP  0x2

// errors
#define DSOS_EFORK  -1
#define DSOS_EWAIT  -2
#define DSOS_ESYSCALL_ARGUMENT_OUT_OF_BOUNDS -3
#define DSOS_ESYSCALL_NOT_IMPLEMENTED -4
#define DSOS_ESYSCALL_OUT_OF_RANGE -5


// syscall numbers
#define DSOS_MAX_SYSCALLS 32
#define DSOS_CALL_PREEMPT   0
#define DSOS_CALL_FORK      1
#define DSOS_CALL_WAIT      2
#define DSOS_CALL_EXIT      3
