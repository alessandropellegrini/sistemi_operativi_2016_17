#pragma once

// this models a fake cpu

#define NUM_REGISTERS 16
typedef struct CPUState{
  unsigned long long rax;
  unsigned long long rdx;
  unsigned long long rcx;
  unsigned long long rbx;
  unsigned long long rsp;
  unsigned long long rbp;
  unsigned long long rsi;
  unsigned long long rdi;
  unsigned long long r8;
  unsigned long long r9;
  unsigned long long r10;
  unsigned long long r11;
  unsigned long long r12;
  unsigned long long r13;
  unsigned long long r14;
  unsigned long long r15;
  unsigned long long rip;
  unsigned long long flags;
	
  // Space for other registers
  unsigned char others[512] __attribute__((aligned(16))); // fxsave wants 16-byte aligned memory
} CPUState;


//fake, copies the stuff from a fake cpu block
void saveCPU(CPUState*);

//fake, copies the stuff to a fake cpu block
void restoreCPU(CPUState*);


