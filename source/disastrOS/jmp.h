// setjmp / longjmp variant which saves all registers

#pragma once
#include "cpu_state.h"

long long _set_jmp(cpu_state *env);
__attribute__ ((__noreturn__)) void _long_jmp(cpu_state *env, long long val);

#define set_jmp(env) 		({\
					int _set_ret;\
					__asm__ __volatile__ ("pushq %rdi"); \
					_set_ret = _set_jmp(env); \
					__asm__ __volatile__ ("add $8, %rsp"); \
					_set_ret;\
				})

#define long_jmp(env, val)	_long_jmp(env, val)
