#pragma once
#include <assert.h>
#include "disastrOS.h"
#include "syscall_parameters.h"

void internal_preempt();

void internal_fork();

void internal_exit();

void internal_wait();
