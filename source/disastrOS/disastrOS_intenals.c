#include "disastrOS.h"

long getSyscallArgument(int argnum) {
  return running->cpu_state.registers[argnum];
}

void setSyscallRetvalue(PCB* pcb, long value) {
  pcb->cpu_state.registers[0]=value;
}



// replaces the running process with the next one in the ready list
void internal_preempt() {
  assert(running);
  // at least one process should be in the running queue
  if (ready_list.first){
    PCB* next_process=(PCB*) List_detach(&ready_list, ready_list.first);
    running->status=Ready;
    List_insert(&ready_list, ready_list.last, (ListItem*) running);
    next_process->status=Running;
    running=next_process;

    /*
    TODO: here we change the address space to the match the one of the running process
          massaging the CPU
   */


  }
}

// creates a new instance of the running_process
// and puts it in the ready list
// returns the pid of the child
void internal_fork(){
  PCB* new_pcb=PCB_alloc();
  if (!new_pcb) {
    setSyscallRetvalue(running, DSOS_EFORK);
    return;
  } 

  /*
    TODO: here we copy memory, descriptors, resources and all the rest
   */

  new_pcb->status=Ready;
  // set the return value for fork() in the child
  setSyscallRetvalue(new_pcb, 0);

  // sets the parent of the newly created process to the running process
  new_pcb->parent=running;
  
  // adds a pointer to the new process to the children list of running
  PCBPtr* new_pcb_ptr=PCBPtr_alloc(running);
  assert(new_pcb_ptr);
  List_insert(&running->children, running->children.last, (ListItem*) new_pcb_ptr);

  //adds the new process to the ready queue
  List_insert(&ready_list, ready_list.last, (ListItem*) new_pcb);

  setSyscallRetvalue(running, new_pcb->pid);
}

// called upon termination
// moves the process to a zombie status
// if the process has children they need to be reparented to
// the init process (pid 1);
// all children should receive SIGHUP
// parent should receive SIGCHLD and be awakened if it was waiting
// for this process to report its value
void internal_exit(){
  
  // 2nd register in pcb contains the exit value
  int return_value=getSyscallArgument(1);
    
  assert(init_pcb);
  while(running->children.first){

    // detach from current list
    ListItem* item = List_detach(& running->children, running->children.first);
    assert(item);
    
    // attach to init's children list
    List_insert(& init_pcb->children, init_pcb->children.last, item);

    // send SIGHUP
    PCBPtr* pcb_ptr=(PCBPtr*) item;
    PCB* pcb=pcb_ptr->pcb;
    pcb->signals |= (DSOS_SIGHUP & pcb->signals_mask);
  }

  /**
     TODO: here we need to release the resources (descriptors and memory)
   */

  running->status=Zombie;
  running->return_value=return_value;
  List_insert(&zombie_list, zombie_list.last, (ListItem*) running);
  running->parent->signals |= (DSOS_SIGCHLD & running->parent->signals_mask);

  // if the parent was waiting for this process to die
  // since he called  wait or waitpid, we wake him up
  if (running->parent->status==Waiting
      && (running->parent->pid_to_wait==0
	  || running->parent->pid_to_wait==running->pid)
      ){
    PCB* parent= (PCB*) List_detach(&waiting_list, (ListItem*) running->parent);
    assert(parent);
    parent->status=Running;

    // we remove the process from the parent's children list
    PCBPtr* self_in_parent=PCBPtr_byPID(&parent->children, running->pid);
    List_detach(&parent->children, (ListItem*) self_in_parent);
    
    // hack the PCB of the parent to put in return value
    setSyscallRetvalue(parent, return_value);

    // the process finally dies
    ListItem* suppressed_item = List_detach(&zombie_list, (ListItem*) running);
    PCB_free((PCB*) suppressed_item);
    
    running=parent;
  } else {
    // we put the first ready process in running 
    PCB* next_running=(PCB*) List_detach(&ready_list, ready_list.first);
    assert(next_running);
    next_running->status=Running;
    running=next_running;
  }
}


void internal_wait(){
  int pid=getSyscallArgument(1);
  int* retval=(int*) getSyscallArgument(2);
  // error conditions:
  // the process has no children
  if (running->children.first == 0){
    setSyscallRetvalue(running, DSOS_EWAIT);
    return;
  }
  running->pid_to_wait=pid;
  
  //  we scan the list of pcbs
  // we stop either
  // - at the first process in zombie status, if pid==0;
  // - at the first process whose pid is the queried one
  PCB* awaited_pcb=0;
  PCBPtr* awaited_pcb_ptr=0;
  ListItem* aux=running->children.first;
  while(aux){
    awaited_pcb_ptr=(PCBPtr*) aux;
    awaited_pcb=awaited_pcb_ptr->pcb;
    if (pid==0 && awaited_pcb->status==Zombie)
      break;
    if (pid>0 && awaited_pcb->pid==pid)
      break;
    aux=aux->next;
  }

  // if we were looking for a process not in child list
  // we need to return an error
  if(pid>0){
    if(! awaited_pcb){
      running->pid_to_wait = -1;
      setSyscallRetvalue(running, DSOS_EWAIT);
      return;
    } 
  }

  // if the pid is in zombie status, we return the value and free the memory
  if (awaited_pcb && awaited_pcb->status==Zombie) {
    // remove the awaited pcb from children list
    List_detach(&running->children, (ListItem*) awaited_pcb_ptr);
    PCBPtr_free(awaited_pcb_ptr);

    *retval=awaited_pcb->return_value;
    // remove he pc from zombie pool
    List_detach(&zombie_list, (ListItem*) awaited_pcb);
    setSyscallRetvalue(running, awaited_pcb->pid);
    PCB_free(awaited_pcb);
    return;
  }

  // all fine, but the process is not a zombie
  // need to sleep
  running->status=Waiting;
  List_insert(&waiting_list, waiting_list.last, (ListItem*) running);

  // pick the next
  PCB* next_running= (PCB*) List_detach(&ready_list, ready_list.first);
  running=next_running;
}
