#pragma once

// veeeery rudimentary for now
typedef struct MemoryInfo{
  void* stack_start;
  int stack_size;
  
  void* heap_start;
  int heap_size;
  
  void* text_start;
  int   text_size;
  
  void* data_start;
  int   data_size;
} MemoryInfo;
