#include "cpu_status.h"
#include "constants.h"

CPUState my_cpu;

//in assembly
void saveCPU(CPUState* s) {
  *s=my_cpu;
}

void restoreCPU(CPUState* s) {
  my_cpu=*s;
}

