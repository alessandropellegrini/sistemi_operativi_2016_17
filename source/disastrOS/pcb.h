#pragma once
#include "constants.h"
#include "double_linked_list.h"
#include "memory_info.h"
#include "cpu_status.h"

typedef enum ProcessStatus {Invalid=-1,
			    Created=0x0,
			    Running=0x1,
			    Ready=0x2,
			    Waiting=0x3,   // io or semaphore wait
			    Suspended=0x4, // ctrl-z
			    Zombie=0x5}
  ProcessStatus;
		    
typedef struct PCB{
  ListItem list;  // MUST BE THE FIRST!!!
  int pid;
  int return_value; // ret value for the parent
  int pid_to_wait;  // -1: none, 0: all, >0 a specific pid
  ProcessStatus status;
  int signals;
  int signals_mask;
  MemoryInfo memory;
  ListHead descriptors;
  struct PCB* parent;
  ListHead children;
  CPUState  cpu_state;
  // more stuff to come
} PCB;

// allocates and initializes a new pcb block
PCB* PCB_alloc();

// frees a pcb block
int PCB_free(PCB* pcb);

// returns a pcb whose pid is pid in the list
PCB* PCB_byPID(ListHead* head, int pid);

// this is a list of *pointers* to pcb used in many cases
typedef struct PCBPtr{
  ListItem list;
  PCB* pcb;
} PCBPtr;

// allocates a list item whose data field
// is a pointer to a PCB
PCBPtr* PCBPtr_alloc(PCB* pcb);

// frees a PCB item
int PCBPtr_free(PCBPtr* pcb);

// returns a pcb whose pid is pid in the list of pointers
PCBPtr* PCBPtr_byPID(ListHead* head, int pid);
