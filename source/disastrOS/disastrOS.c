#include <assert.h>
#include <stdarg.h>
#include "pcb_internals.h"
#include "disastrOS.h"
#include "disastrOS_internals.h"


PCB* init_pcb;
PCB* running;
int last_pid;
ListHead ready_list;
ListHead waiting_list;
ListHead zombie_list;

// a resource can be a device, a file or an ipc thing
ListHead resources_list;

typedef void(*SyscallFunctionType)();
SyscallFunctionType syscall_vector[DSOS_MAX_SYSCALLS];
int syscall_numarg[DSOS_MAX_SYSCALLS];

extern CPUState my_cpu; // our fake cpu

void my_system_trap() {
  // here we pretend to be in the system
  // we save all registers of our fake CPU on the running pcb
  saveCPU(&running->cpu_state);

  // we retrieve the system call number from first register
  int syscall_num=getSyscallArgument(0);
  if (syscall_num<0||syscall_num>DSOS_MAX_SYSCALLS)
    setSyscallRetvalue(running, DSOS_ESYSCALL_OUT_OF_RANGE);

  SyscallFunctionType my_syscall=syscall_vector[syscall_num];

  if (! my_syscall)
    setSyscallRetvalue(running, DSOS_ESYSCALL_NOT_IMPLEMENTED);
  (*syscall_vector[syscall_num])();

  
  //at exit we restore all registers from the running pcb (which might have changed)
  restoreCPU(&running->cpu_state);
}

long disastrOS_syscall(long syscall_num, ...) {
  va_list ap;
  if (syscall_num<0||syscall_num>DSOS_MAX_SYSCALLS)
    return DSOS_ESYSCALL_OUT_OF_RANGE;

  int nargs=syscall_numarg[syscall_num];
  va_start(ap,syscall_num);

  // this is seriously horrible :)
  // here we put the stuff in the cpu registers
  for (int i=0; i<nargs; ++i){
    setSyscallArgument(i+1, va_arg(ap,long));
  }
  va_end(ap);
  setSyscallArgument(0, syscall_num);

  // we stuffed the CPU, we call the system

  my_system_trap(); 

  // funky convention for syscalls
  return getSyscallArgument(0);
}

void disastrOS_init(){
  PCB_init();
  init_pcb=0;

  // populate the vector of syscalls and number of arguments for each syscall
  for (int i=0; i<DSOS_MAX_SYSCALLS; ++i){
    syscall_vector[i]=0;
  }
  syscall_vector[DSOS_CALL_PREEMPT]   = internal_preempt;
  syscall_numarg[DSOS_CALL_PREEMPT]   = 0;
  
  syscall_vector[DSOS_CALL_FORK]      = internal_fork;
  syscall_numarg[DSOS_CALL_FORK]      = 0;
  
  syscall_vector[DSOS_CALL_WAIT]      = internal_wait;
  syscall_numarg[DSOS_CALL_WAIT]      = 2;

  syscall_vector[DSOS_CALL_EXIT]      = internal_exit;
  syscall_numarg[DSOS_CALL_EXIT]      = 1;

  // setup the scheduling lists
  running=0;
  List_init(&ready_list);
  List_init(&waiting_list);
  List_init(&zombie_list);
  List_init(&resources_list);
}

void disastrOS_start(){
  running=PCB_alloc();
  running->status=Running;
  init_pcb=running;
}

long disastrOS_fork(){
  return disastrOS_syscall(DSOS_CALL_FORK);
}

long disastrOS_wait(long pid, long *retval){
  return disastrOS_syscall(DSOS_CALL_WAIT, pid, retval);
}

void disastrOS_exit(long exitval) {
  disastrOS_syscall(DSOS_CALL_EXIT, exitval);
}

void disastrOS_preempt(){
  disastrOS_syscall(DSOS_CALL_PREEMPT);
}
